<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>
		<main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>Consent</h1>
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">Consent</li>
							</ol>
						</nav>
                        <!--<div class="top-right-button-container">

                            <div class="btn-group">
                                <div class="btn btn-primary btn-lg pl-4 pr-0 check-button">
                                    <label class="custom-control custom-checkbox mb-0 d-inline-block">
                                        <input type="checkbox" class="custom-control-input" id="checkAll">
                                        <span class="custom-control-label">&nbsp;</span>
                                    </label>
                                </div>
                                <button type="button"
                                    class="btn btn-lg btn-primary dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                </div>
                            </div>
                        </div>-->
                    </div>

                    <?php /*?><div class="mb-2 d-flex justify-content-between">
						<div class="col-l">
							<a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
								role="button" aria-expanded="true" aria-controls="displayOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="collapse d-md-block" id="displayOptions">
								<div class="d-flex align-items-end">

									<div class="float-md-left mr-2 mb-1 dropdown-as-select">
										<label class="d-block">Conset Type :</label>
										<button class="btn bg-white btn-xs dropdown-toggle" type="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											All Consent
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item active" href="#">All System</a>
											<a class="dropdown-item" href="#">Another action</a>
										</div>
									</div>
									
									<div class="input-daterange no-gutters d-flex flex-nowrap align-items-end mr-2 mb-1" id="datepicker-report"> 
										<div class="col mb-0">
											<label class="d-block">Start date :</label>
											<input type="text" class="form-control form-control-sm bg-white rounded-2" name="Select date"
														placeholder="Start" value="05/04/2020" />
										</div>
										<span class="form-group pl-2 pr-2 mb-0"><label>to</label></span>
										<div class="col mb-0">
											<label class="d-block">End date :</label>
											<input type="text" class="form-control form-control-sm bg-white rounded-2" name="Select date"
														placeholder="End" value="05/15/2020" />
										</div>
									</div>

	
									<div class="float-md-left mr-1 mb-1">
										
										<button class="btn btn-primary btn-xs text-white" type="button">
											Search
										</button>
										
									</div>

								</div>
							</div>
						</div>
						<div class="col-r pb-2 top-right-button-container d-flex align-items-end">
              <a href="/consent-add" class="btn btn-primary btn-md top-right-button  mr-1">+ Add</a>
						</div>
					</div><?php */?>
					
					<div class="srh-bar mb-4 d-flex justify-content-between flex-row flex-nowrap">
						<div class="col p-0 pl-1 pr-0">
							<a class="btn p-2 d-inline-block d-md-none" data-toggle="collapse" href="#searchOptions" role="button" aria-expanded="true" aria-controls="searchOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="card collapse d-md-block" id="searchOptions">
								<div class="card-body p-3 d-flex flex-wrap justify-content-between h-100 align-items-end">
									<div class="d-flex flex-wrap w-100 mb-3">
									
										<div class="input-daterange col-12 col-sm form-group mb-0">
											<label>Summitted Date</label>
											<div class="row mb-0 align-items-center">
												<div class="col pr-2">
													<div class="input-group">
															<span class="input-group-text input-group-append input-group-addon border-right-0">
																<i class="simple-icon-calendar"></i>
															</span>
															<input type="text" class="input-sm form-control border-left-0" name="start" placeholder="Start date">
														</div>

												</div>
												<div class="font-weight-bold">To</div>
												<div class="col pl-2">
													<div class="input-group">
															<span class="input-group-text input-group-append input-group-addon border-right-0">
																<i class="simple-icon-calendar"></i>
															</span>
															<input type="text" class="input-sm form-control border-left-0" name="end" placeholder="End date">
														</div>
												</div>
											</div>
										</div>
										
										<div class="input-daterange col-12 col-sm form-group mb-0">
											<label>Consent Type</label>
											<div class="row mb-0 align-items-center">
												<div class="col pr-2">
													<div class="input-group">
															<span class="input-group-text input-group-append input-group-addon border-right-0">
																<i class="simple-icon-calendar"></i>
															</span>
															<input type="text" class="input-sm form-control border-left-0" name="start" placeholder="Start date">
														</div>

												</div>
												<div class="font-weight-bold">To</div>
												<div class="col pl-2">
													<div class="input-group">
															<span class="input-group-text input-group-append input-group-addon border-right-0">
																<i class="simple-icon-calendar"></i>
															</span>
															<input type="text" class="input-sm form-control border-left-0" name="end" placeholder="End date">
														</div>
												</div>
											</div>
										</div>
										
										
									</div>

									<div class="d-flex flex-wrap w-100 align-items-end">
										<div class="col-12 col-sm form-group mb-2">
											<label>Consent Type</label>
											<select class="form-control select2-single" data-width="100%" data-placeholder="Select Consent Type" data-minimum-results-for-search="Infinity">
												<option></option>
												<option>Private</option>
												<option>Publish</option>
											</select>

										</div>
										
										<div class="col-12 col-sm form-group mb-2">
											<label>Consent Status</label>
											<select class="form-control select2-single" data-width="100%" data-placeholder="Select Consent Status" data-minimum-results-for-search="Infinity">
												<option></option>
												<option>Active</option>
												<option>Draft</option>
											</select>

										</div>


										<div class="top-right-button-container text-nowrap col-12 col-sm-auto mb-2">

											<button class="btn btn-primary btn-md top-right-button rounded-05" type="button" id="btnSearch" style="min-width: 120px"> <!--<i class="icon-img"><img src="di/ic-search-wh.png" height="20"></i>--> Search</button>
										</div>
										
									</div>

								</div>
							</div>
						</div>

					</div>
					
					<div class="headbar-tb mb-4 d-flex justify-content-end align-items-center">
						<div class="col-r top-right-button-container d-flex align-items-end">
							<a href="/consent-add" class="btn btn-primary btn-md top-right-button  mr-1">+ Add Consent</a>
						</div>
					</div>

                    <div class="separator mb-5"></div>

					
					<div class="card main-consent-setting">
					<div class="card-body">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
							<!--<table id="tablelist" class="data-table data-table-feature">-->
							<!--<table id="tablelist" class="data-table data-table-standard">-->
                           <table class="data-tables-pagination dataTable no-footer responsive nowrap">
							
									<thead>
										<tr>
											<th>No.</th>
											<th>Type</th>
											<th>title</th>
											<th>Peroid</th>
											<th>Current Version</th>
											<th class="text-center sort-none">Status</th>
											<th class="text-center sort-none" width="20%">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php for($i=1;$i<=20;$i++){ ?>
										<tr>
											<td><?php echo $i; ?></td>
											<td>Publish</td>
											<td>
												<a class="text-dark" href="#">
													consent	<?php echo $i; ?> 						
												</a>
											</td>
											<td>01Jan2020-31Dec2020</td>
											<td>1.0.<?php echo $i; ?></td>
											<td class="text-center">
												
												<?php if($i%5==0){ ?><span class="text-warning font-weight-semibold">Draft</span>
												<?php } else { ?><span class="text-primary font-weight-semibold">Active</span><?php } ?>
											</td>
											<td class="text-center">
												<a href="/consent-detail" class="btn btn-primary mr-2 btn-sm">View</a>
												<a href="#" class="btn btn-primary mr-2 btn-sm">Edit</a>
												<a href="#" class="btn btn-outline-danger btn-sm">Delete</a>
											  </td>
										</tr>
										<?php } ?>

                   <?php /*?> <tr>
											<td>2</td>
											<td>Private</td>
											<td>
												<a class="text-dark" href="#">
													MBK loan system management													
												</a>
											</td>
											<td>-</td>
											<td class="text-center"><span class="text-primary font-weight-semibold">Active</span></td>
											<td class="text-center">
                        <a href="/consent-detail" class="btn btn-primary mr-2 btn-sm">View</a>
                        <a href="#" class="btn btn-primary btn-sm">Edit</a>
                      </td>
										</tr>

                    <tr>
											<td>3</td>
											<td>Private</td>
											<td>
												<a class="text-dark" href="#">
													MBK loan system management													
												</a>
											</td>
											<td>-</td>
											<td class="text-center"><span class="text-primary font-weight-semibold">Active</span></td>
											<td class="text-center">
                        <a href="/consent-detail" class="btn btn-primary mr-2 btn-sm">View</a>
                        <a href="#" class="btn btn-primary btn-sm">Edit</a>
                      </td>
										</tr>

                    <tr>
											<td>4</td>
											<td>Publish</td>
											<td>
												<a class="text-dark" href="#">
													MBK loan system management													
												</a>
											</td>
											<td>-</td>
											<td class="text-center"><span class="text-dark-50 font-weight-semibold">inactive</span></td>
											<td class="text-center">
                        <a href="/consent-detail" class="btn btn-primary mr-2 btn-sm">View</a>
                        <a href="#" class="btn btn-primary btn-sm">Edit</a>
                      </td>
										</tr><?php */?>
																				

									</tbody>
								</table>
								
		

						</div>
						
						<?php /*?><nav class="mt-4 mb-3">
                        <ul class="pagination justify-content-center mb-0">
                            <li class="page-item ">
                                <a class="page-link first" href="#">
                                    <i class="simple-icon-control-start"></i>
                                </a>
                            </li>
                            <li class="page-item ">
                                <a class="page-link prev" href="#">
                                    <i class="simple-icon-arrow-left"></i>
                                </a>
                            </li>
                            <li class="page-item active">
                                <a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item ">
                                <a class="page-link" href="#">2</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">3</a>
                            </li>
                            <li class="page-item ">
                                <a class="page-link next" href="#" aria-label="Next">
                                    <i class="simple-icon-arrow-right"></i>
                                </a>
                            </li>
                            <li class="page-item ">
                                <a class="page-link last" href="#">
                                    <i class="simple-icon-control-end"></i>
                                </a>
                            </li>
                        </ul>
                    </nav><?php */?>
					</div>
					</div>
                </div>
            </div>
        </div>

    </main>

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
	<script>
$(document).ready(function() {
    $('.select2-single').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		
	});
} );
	</script>
</body>

</html>