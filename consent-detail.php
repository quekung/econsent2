<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>
		<main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>Consent</h1>
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="consetn-list">Consent</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">View</li>
							</ol>
						</nav>

            </div>

          <div class="mb-2 d-flex justify-content-between align-items-center">
						<div class="col-l">
							<h2 class="sub-head font-weight-bold text-medium mb-0">Info</h2>
						</div>
						
					</div>

          <div class="card mb-4">
            <div class="card-body">
              <div class="row mb-0">
                  <div class="col-sm-6">
                    <p class="text-muted text-small mb-1">Consent Title:</p>
                    <p>
                      MBK Loan system management 
                    </p>
                  </div>
                  <div class="col-sm-6">
                    <p class="text-muted text-small mb-1">Consent Version:</p>
                    <p>
                      5.0
                    </p>
                  </div>
              </div>
              <div class="row mb-0">
                  <div class="col">
                    <p class="text-muted text-small mb-1">Purpose of Consent :</p>
                    <p>
                      ตกลงยินยอมให้ บมจ.xxxx  เปิดผข้อมูลส่วนบุกกของช้พจ เช่น ชื่อ ช่องทางในการติดต่อเพศอายุการศึกษา เป็นต้น แต่ทั้งนี้ ไม่รวมถึงข้อมูลที่เกี่ยวกับบัญชีของข้พเจ้าเช่น เลขที่บัญชี ยอดคงเหลือในบัญชี รายการเคลื่อนไหวในบัญชีเป็นต้น ให้แก่กลุ่มธุรกิจทางการเงินของธนาคารและให้กลุ่มธุรกิจทางการเงินของธนาคารสามารถใช้ข้มูลดังกล่ว พื่อวัตถุประสงค์ทางการตลาด เช่น เพื่อวัตถุประสงค์ในการพิจารณานำเสนอผลิตภัณฑ์ หรือเพื่อส่งเสริมการขายผลิตภัณฑ์บริกร และข้อสนอพิเศษอื่นๆ ของบริษัทในกลุ่มธุรกิจการเงินของธนาคาร ให้แก่ข้าพเจ้า
                    </p>
                  </div>
                  
              </div>
              <div class="row mb-0">
                  <div class="col-sm-4">
                    <p class="text-muted text-small mb-1">Consent Start Date:</p>
                    <p>
                      19-05-2020
                    </p>
                  </div>
                  <div class="col-sm-4">
                    <p class="text-muted text-small mb-1">Consent Expire Date:</p>
                    <p>
                      19-05-2023
                    </p>
                  </div>
                </div>



            </div>
          </div>

          <div class="mb-2 d-flex justify-content-between align-items-center">
						<div class="col-l">
							<h2 class="sub-head font-weight-bold text-medium mb-0">Version</h2>
						</div>
						<div class="col-r pb-2 top-right-button-container d-flex align-items-end">
                            <a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#consentAddPop" class="btn btn-primary btn-md top-right-button mr-1">+ Add</a>
								
						</div>
					</div>


					<div class="card main-consent-setting">
					<div class="card-body">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
							<!--<table id="tablelist" class="data-table data-table-feature">-->
							<!--<table id="tablelist" class="data-table data-table-standard">-->
                           <table class="data-table data-tables-pagination responsive nowrap">
							
									<thead>
										<tr>
											<th class="text-center pr-0">No.</th>
											<th>Version</th>
											<!--th class="text-center pr-0">Status</th-->
											<th class="text-center sort-none" width="25%">Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-center">1</td>
											<td>
												<a class="text-dark" href="#">
													XXY													
												</a>
											</td>
											<!--td class="text-center"><span class="text-primary font-weight-semibold">Active</span></td-->
											<td class="text-center">
											<a href="#" class="btn btn-primary btn-sm mr-2">View</a> 
											<a href="#" class="btn btn-primary btn-sm mr-2">Edit</a> 
											<a href="javascript:;" class="btn btn-outline-primary btn-sm">Delete</a>
											</td>
										</tr>

                    <tr>
											<td class="text-center">2</td>
											<td>
												<a class="text-dark" href="#">
													XXX													
												</a>
											</td>
											<!--td class="text-center"><span class="text-primary font-weight-semibold">Active</span></td-->
											<td class="text-center">
											<a href="#" class="btn btn-primary btn-sm mr-2">View</a> 
											<a href="#" class="btn btn-primary btn-sm mr-2">Edit</a> 
											<a href="javascript:;" class="btn btn-outline-primary btn-sm">Delete</a>
											</td>
										</tr>

                    <tr>
											<td class="text-center">3</td>
											<td>
												<a class="text-dark" href="#">
													XXX													
												</a>
											</td>
											<!--td class="text-center"><span class="text-primary font-weight-semibold">Active</span></td-->
											<td class="text-center">
											<a href="#" class="btn btn-primary btn-sm mr-2">View</a> 
											<a href="#" class="btn btn-primary btn-sm mr-2">Edit</a> 
											<a href="javascript:;" class="btn btn-outline-primary btn-sm">Delete</a>
											</td>
										</tr>



									
																				

									</tbody>
								</table>
								
		

						</div>
					</div>
					</div>
                </div>
            </div>
        </div>

    </main>

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
</body>

</html>