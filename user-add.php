<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>
		<main>
    <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>User Management</h1>
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="/user-list">User</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Add User </li>
							</ol>
						</nav>

                    </div>



                    <div class="mb-3"></div>
					
				
					<div class="d-flex justify-content-center mb-4">

						
					
					<div class="col-sm-10 col-md-8 col-lg-6">
					<div class="card h-100 d-flex justify-content-center flex-row">
					<div class="card-body">
            <div class="row">
							<div class="wr col-sm-6 mb-4">
								<label>Username :</label>
								<input type="text" class="form-control" placeholder="">
							</div>

              <div class="wr col-sm-6 mb-4">
								<label>Password :</label>
								<input type="password" class="form-control" placeholder="">
							</div>

              <div class="wr col-sm-6 mb-4">
								<label>Firstname :</label>
								<input type="text" class="form-control" placeholder="">
							</div>

              <div class="wr col-sm-6 mb-4">
								<label>Lastname :</label>
								<input type="text" class="form-control" placeholder="">
							</div>

              <div class="wr col-sm-6 mb-4">
								<label>Email :</label>
								<input type="email" class="form-control" placeholder="">
							</div>
							
							<div class="wr col-sm-6 mb-4">
								<label>Phone :</label>
								<input type="tel" class="form-control" placeholder="">
							</div>

							<div class="wr col-sm-6 mb-4">
								<label>Status :</label>
								<select class="form-control select2-single" data-width="100%">
									<option value="0" selected>Active</option>
									<option value="1">Disable</option>
								</select>
							</div>
						</div>								
		
														
						<div class="d-flex justify-content-between mb-4">
							
								
								<button type="button" class="btn btn-gray btn-sm mb-2 mr-1 min-w">
								
								Reset</button>

                <button type="button" class="btn btn-primary btn-sm mb-2 ml-1 min-w">
								
								Save</button>
						</div>
					</div>
					</div>
					</div>
					</div>
					
					
					
					
					
                </div>
            </div>
        </div>
  </main>
    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
</body>

</html>