<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>
<main>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="mb-2">
					<h1>Customer</h1>
					<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
						<ol class="breadcrumb pt-0">
							<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
                  </li>-->
							<li class="breadcrumb-item">
								<a href="consetn-list">Customer</a>
							</li>
							<li class="breadcrumb-item active text-gray" aria-current="page">View</li>
						</ol>
					</nav>
				</div>

				<div class="mb-2 d-flex justify-content-between align-items-center">
					<div class="col-l">
						<h2 class="sub-head font-weight-bold text-medium mb-0">Info</h2>
					</div>
				</div>

				<div class="card mb-4">
					<div class="card-body">
						<div class="row mb-0">
							<div class="col-sm-6">
								<p class="text-muted text-small mb-1">Customer Name:</p>
								<p>Sarawut P</p>
							</div>
							<div class="col-sm-6">
								<p class="text-muted text-small mb-1">Phone:</p>
								<p>
									<a href="tel:0811112222" class="text-primary">081-111-2222</a>
								</p>
							</div>
						</div>

						<div class="row mb-0">
							<div class="col-sm-6">
								<p class="text-muted text-small mb-1">Email:</p>
								<p>
									<a href="mailto:sarun.s@gmail.com" class="text-primary">sarun.s@gmail.com</a>
								</p>
							</div>

							<div class="col-sm-6">
								<p class="text-muted text-small mb-1">Address :</p>
								<p>437/109 ลุมพินีคอนโด ถนนรัตนาธิเบศร์</p>
							</div>
						</div>
						<!-- 
                <div class="separator mb-3"></div>

                <div class="row mb-0">
                  <div class="col-sm-6 col-xl-4">
                    <p class="text-muted text-small mb-1">Address :</p>
                    <p>437/109 ลุมพินีคอนโด ถนนรัตนาธิเบศร์</p>
                  </div>
                </div> -->
					</div>
				</div>
				<!-- tab -->
				<div class="tab-zone">
					<div class="head-tab-title">
						<ul class="nav nav-tabs separator-tabs ml-0 mb-0" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="consent-tab" data-toggle="tab" href="#consent" role="tab" aria-controls="first" aria-selected="true">Consent List</a>
							</li>

							<li class="nav-item">
								<a class="nav-link" id="privacy-tab" data-toggle="tab" href="#privacy" role="tab" aria-controls="third" aria-selected="false">Privacy Notice List</a>
							</li>
						</ul>
						<!--<div class="fix-r">
						<a class="col-12 btn btn-primary btn-sm" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#addConsent"> + Add Consent</a>
					</div>-->
					</div>
					<div class="tab-content">
						<!-- tab1 -->
						<div class="tab-pane show active" id="consent" role="tabpanel" aria-labelledby="consent-tab">
							<div class="mb-4 mt-n5 d-flex justify-content-end align-items-center">
								<div class="col-r pb-2 top-right-button-container d-flex align-items-end">
									<a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#customerAddPop" class="btn btn-primary btn-md top-right-button mr-1">+ Add</a>
								</div>
							</div>

							<div class="card main-consent-setting">
								<div class="card-body">
									<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
										<!--<table id="tablelist" class="data-table data-table-feature">-->
										<!--<table id="tablelist" class="data-table data-table-standard">-->
										<table class="data-table data-tables-pagination responsive nowrap">
											<thead>
												<tr>
													<th>No.</th>
													<th>Channel</th>
													<th>Consent Type</th>
													<th>Type</th>
													<th>Consent</th>
													<th width="15%">Version</th>
													<!--th class="text-center pr-0">Status</th-->
													<th class="text-center sort-none" width="25%">Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1</td>
													<td>Chatbot</td>
													<td>Consent</td>
													<td class="text-center2">Public</td>
													<td>
														<a class="text-dark" href="#">NTL loan system management</a>
													</td>
													<td>1.0.0</td>
													<!--td class="text-center"><span class="text-primary font-weight-semibold">Active</span></td-->
													<td class="text-center">
														<a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#customerEditPop" class="btn btn-primary btn-sm mr-2">View</a>
														<a href="javascript:;" class="btn btn-outline-primary btn-sm">Revoke</a>
													</td>
												</tr>
												<tr>
													<td class="text-center">1</td>
													<td>Web</td>
													<td>Super Consent</td>
													<td class="text-center2">Private</td>
													<td>
														<a class="text-dark" href="#">NTL loan system management2</a>
													</td>
													<td>1.0.1</td>
													<!--td class="text-center"><span class="text-primary font-weight-semibold">Active</span></td-->
													<td class="text-center">
														<a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#customerEditPop" class="btn btn-primary btn-sm mr-2">View</a>
														<a href="javascript:;" class="btn btn-outline-primary btn-sm">Revoke</a>
													</td>
												</tr>

											</tbody>
										</table>
									</div>
								</div>
							</div>

						</div>
						<!-- tab2 -->
						<div class="tab-pane fade" id="privacy" role="tabpanel" aria-labelledby="privacy-tab">
							<div class="mb-4 mt-n5 d-flex justify-content-end align-items-center">

								<div class="col-r pb-2 top-right-button-container d-flex align-items-end">
									<a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#privacyAddPop" class="btn btn-primary btn-md top-right-button mr-1">+ Add</a>
								</div>
							</div>

							<div class="card main-consent-setting">
								<div class="card-body">
									<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
										<!--<table id="tablelist" class="data-table data-table-feature">-->
										<!--<table id="tablelist" class="data-table data-table-standard">-->
										<table class="data-table data-tables-pagination responsive nowrap">
											<thead>
												<tr>
													<th>No.</th>
													<th>Channel</th>
													<th>Type</th>
													<th>Privacy title</th>
													<th width="15%">Version</th>
													<!--th class="text-center pr-0">Status</th-->
													<th class="text-center sort-none" width="15%">Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1</td>
													<td>Chatbot</td>
													<td>Public</td>
													<td>
														<a class="text-dark" href="#">1.0.0.1</a>
													</td>
													<td>5.0</td>
													<!--td class="text-center"><span class="text-primary font-weight-semibold">Active</span></td-->
													<td class="text-center">
														<a href="javascript:;" class="btn btn-outline-primary btn-sm">Revoke</a>
													</td>
												</tr>

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- /tab -->





			</div>
		</div>
	</div>

	<!-- add modal -->
	<div class="modal fade show" id="customerAddPop">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add New</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
				
				</div>
				<form>
					<div class="modal-body">
						<div class="form-group cv-select-cr">
							<label>Channal :</label>
							<select class="form-control select2-single" data-width="100%">
								<option label="&nbsp;">All Channal</option>
								<option value="Option 1">Chatbot</option>
								<option value="Option 2">Web</option>
							</select>
						</div>

						<div class="form-group cv-select-cr">
							<label>Consent Type :</label>
							<select v-model="consentType" class="form-control select2-single" data-width="100%">
								<option label="&nbsp;">All consent category</option>
								<option value="consent">Consent</option>
								<option value="superconsent">Super Consent</option>
							</select>
						</div>

						<div class="form-group cv-select-cr">
							<label>Type :</label>
							<select class="form-control select2-single" data-width="100%">
								<option value="Option 1">Public</option>
								<option value="Option 2">Private</option>
								<option value="Option 3">Option 3</option>
							</select>
						</div>


						<div class="form-group">
							<label>Consents :</label><br/>
							<span>consent 1 <br /><input type="radio" name="a" /> Accept <input type="radio" name="a" > No </span> <br/>
							<span>consent 2 <br /><input type="radio"> Accept <input type="radio"> No </span> <br/>
							<span>consent 3 <br /><input type="radio"> Accept <input type="radio"> No </span> <br/>
							<span>consent 4 <br /><input type="radio"> Accept <input type="radio"> No </span> <br/>
							<span>consent 5 <br /><input type="radio"> Accept <input type="radio"> No </span> <br/>
							<span>consent 6 <br /><input type="radio"> Accept <input type="radio"> No </span> <br/>
						</div>

						<div class="form-group">
							<label>Method :</label>
							<input type="text" class="form-control" value="">
						</div>

						<div class="form-group">
							<label>Region :</label>
							<input type="text" class="form-control" value="">
						</div>

						<div class="form-group">
							<label>Area :</label>
							<input type="text" class="form-control" value="">
						</div>

						<div class="form-group">
							<label>Branch :</label>
							<input type="text" class="form-control" value="">
						</div>

						<div class="form-group">
							<label>REF no :</label>
							<input type="text" class="form-control" value="">
						</div>

						<div class="form-group">
							<label>Contact no :</label>
							<input type="text" class="form-control" value="">
						</div>

						<div class="form-group">
							<label>Ticket no :</label>
							<input type="text" class="form-control" value="">
						</div>

						<div class="form-group">
							<label>Remark :</label>
							<textarea class="form-control" rows="2" name="jQueryDetail" required></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /add modal -->

	<!-- Edit modal -->
	<div class="modal fade show" id="customerEditPop">

		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">View</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
				
				</div>
				<form>
					<div class="modal-body">
						<div class="form-group cv-select-cr">
							<label>Channal :</label>
							<select class="form-control select2-single" data-width="100%">
								<option value="Option 1" selected>Chatbot</option>
								<option value="Option 2">Web</option>
							</select>
						</div>

						<div class="form-group cv-select-cr">
							<label>Consent Type :</label>
							<select v-model="consentType" class="form-control select2-single" data-width="100%">
								<option label="&nbsp;">All consent category</option>
								<option value="consent" selected>Consent</option>
								<option value="superconsent">Super Consent</option>
							</select>
						</div>

						<div class="form-group cv-select-cr">
							<label>Type :</label>
							<select class="form-control select2-single" data-width="100%">
								<option value="Option 1" selected>Public</option>
								<option value="Option 2">Private</option>
								<option value="Option 3">Option 3</option>
							</select>
						</div>


						<div class="form-group">
							<label>Consents :</label><br/>
							<span>consent 1 </span> <button type="button" class="btn btn-danger btn-sm btn-xs">revoke</button> <br/><br/>
							<span>consent 2 </span> <button type="button" class="btn btn-danger btn-sm btn-xs">revoke</button> <br/><br/>
							<span>consent 3 </span> <button type="button" class="btn btn-danger btn-sm btn-xs">revoke</button> <br/><br/>
							<span>consent 4 </span> <button type="button" class="btn btn-danger btn-sm btn-xs">revoke</button> <br/><br/>
							<span>consent 5 </span> <button type="button" class="btn btn-danger btn-sm btn-xs">revoke</button> <br/><br/>
							<span>consent 6 </span> <button type="button" class="btn btn-danger btn-sm btn-xs">revoke</button> <br/><br/>
						</div>

						<div class="form-group">
							<label>Method :</label>
							<input type="text" class="form-control" value="">
						</div>

						<div class="form-group">
							<label>Region :</label>
							<input type="text" class="form-control" value="">
						</div>

						<div class="form-group">
							<label>Area :</label>
							<input type="text" class="form-control" value="">
						</div>

						<div class="form-group">
							<label>Branch :</label>
							<input type="text" class="form-control" value="">
						</div>

						<div class="form-group">
							<label>REF no :</label>
							<input type="text" class="form-control" value="">
						</div>

						<div class="form-group">
							<label>Contact no :</label>
							<input type="text" class="form-control" value="">
						</div>

						<div class="form-group">
							<label>Ticket no :</label>
							<input type="text" class="form-control" value="">
						</div>

						<div class="form-group">
							<label>Remark :</label>
							<textarea class="form-control" rows="2" name="jQueryDetail" required></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>

	</div>
	<!-- /Edit modal -->

	<!-- add modal -->
	<div class="modal fade show" id="privacyAddPop">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add New</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
				
				</div>
				<form>
					<div class="modal-body">
						<div class="form-group cv-select-cr">
							<label>Channel :</label>
							<select class="form-control select2-single" data-width="100%">
								<option label="&nbsp;">All Channal</option>
								<option value="Option 1">Chatbot</option>
								<option value="Option 2">Web</option>
							</select>
						</div>

						<div class="form-group">
							<label>Consents :</label><br/>
							<input type="radio" name="a"/> Accept <input type="radio" name="a"> No </span> <br/>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /add modal -->
</main>
    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
</body>

</html>