<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>Consent Setting</h1>
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">Consent Subcategory Management  </li>
							</ol>
						</nav>

                    </div>

                    <div class="mb-2 d-flex justify-content-end">

						<div class="col-r pb-2 top-right-button-container d-flex align-items-end">
                            <a href="add-consent-subcat.php" class="btn btn-primary btn-md top-right-button mr-1">+ Add Consent Subcategory</a>
								
						</div>
					</div>

                    <div class="mb-3"></div>
					<div class="card main-consent-setting">
					<div class="card-body">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
							<!--<table id="tablelist" class="data-table data-table-feature">-->
							<!--<table id="tablelist" class="data-table data-table-standard">-->
                           <table class="data-table data-table-scrollable responsive nowrap" data-order="[[ 1, &quot;desc&quot; ]]">
							
									<thead>
										<tr>
											<th class="text-center">No.</th>
											<th>Consent Subcategory</th>
											
											<th class="text-center">Status</th>
											<th class="sort-none">&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										<?php for($i=1;$i<=30;$i++){ ?>
										<tr>
											<td class="text-center"><?php echo $i ?></td>
											<td>
												<a class="text-dark" href="consent-subcategory.php">
													การยินยอมขอรับเงินกู้ตามกรมธรรม์หรือ ผลประโยชน์ตามกรมธรรมผ่านบัญชี
													
												</a>
											</td>
											
											<td class="text-center"><span class="text-primary font-weight-semibold">Active</span></td>
											<td class="text-center"><a href="consent-setting-edit.php" class="btn btn-primary btn-sm mr-2">Edit</a> <a href="javascript:;" class="btn btn-outline-primary btn-sm">Delete</a></td>
										</tr>
										<?php } ?>
										<!-- modal -->
										<div class="modal fade" id="viewProfile" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title">Profile</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">×</span>
													</button>
												</div>
												<div class="modal-body">
													<ul class="list-inline">
														<li class="mb-4">
															<a class="p-0 d-flex align-items-center text-dark" href="#">
																<span class="mr-2">
																	<img alt="Profile Picture" src="img/profile-pic-l.jpg" class="rounded-circle" height="100">
																</span>
																<h2 class="name text-left">
																	เอสเธอร์ อเล็กซานเดอร์
																	<small class="d-block">Esther Alexander</small>
																</h2>

															</a>
														</li>
														<li>
															<div class="form-group">
																<label>Email</label>
																<input type="text" class="form-control" placeholder="" value="lillie.foster@example.com">
															</div>
														</li>
														<li>
															<div class="form-group">
																<label>Mobile Number</label>
																<input type="text" class="form-control" placeholder="" value="302.555.0107">
															</div>
														</li>
														<li>
															<div class="form-group">
																<label>ID Card</label>
																<input type="text" class="form-control" placeholder="" value=" 	6783776898788">
															</div>
														</li>
														
													</ul>

												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
													<button type="button" class="btn btn-primary">Save changes</button>
												</div>
											</div>
										</div>
									</div>
									<!-- /modal -->

									</tbody>
								</table>
								
		

						</div>
					</div>
					</div>
                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
<script>
$(document).ready(function() {
	$('.main-menu .list-unstyled>li').removeClass('active');
	$('.main-menu .list-unstyled>li:nth-child(3)').addClass('active');
});
</script>
</body>

</html>