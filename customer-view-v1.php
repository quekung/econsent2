<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>
		<main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>Customer</h1>
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="consetn-list">Customer</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">View</li>
							</ol>
						</nav>

            </div>

          <div class="mb-2 d-flex justify-content-between align-items-center">
						<div class="col-l">
							<h2 class="sub-head font-weight-bold text-medium mb-0">Info</h2>
						</div>
						
					</div>

          <div class="card mb-4">
            <div class="card-body">
              <div class="row mb-0">
                  <div class="col-sm-6">
                    <p class="text-muted text-small mb-1">Customer Name:</p>
                    <p>
                      MBK Loan system management 
                    </p>
                  </div>
                  <div class="col-sm-6">
                    <p class="text-muted text-small mb-1">Phone:</p>
                    <p>
                      <a href="tel:0811112222" class="text-primary">081-111-2222</a>
                    </p>
                  </div>
              </div>
              
              <div class="row mb-0">
                  <div class="col-sm-6">
                    <p class="text-muted text-small mb-1">Email:</p>
                    <p>
                      <a href="mailto:sarun.s@gmail.com" class="text-primary">sarun.s@gmail.com </a>
                    </p>
                  </div>
                  
              </div>

              <div class="separator mb-3"></div>

              <div class="row mb-0">
                <div class="col-sm-6 col-xl-4">
                  <p class="text-muted text-small mb-1">Address Line1 :</p>
                  <p>
                    437/109 ลุมพินีคอนโด  ถนนรัตนาธิเบศร์
                  </p>
                </div>
                <div class="col-sm-6 col-xl-4">
                  <p class="text-muted text-small mb-1">Address Line2 :</p>
                  <p>
                    -
                  </p>
                </div>
  
                <div class="col-sm-6 col-xl-4">
                  <p class="text-muted text-small mb-1">Sub District :</p>
                  <p>
                    บางกระสอ
                  </p>
                </div>
                <div class="col-sm-6 col-xl-4">
                  <p class="text-muted text-small mb-1">District :</p>
                  <p>
                    เมืองนนทบุรี
                  </p>
                </div>

                <div class="col-sm-6 col-xl-4">
                  <p class="text-muted text-small mb-1">Province :</p>
                  <p>
                    นนทบุรี
                  </p>
                </div>
                <div class="col-sm-6 col-xl-4">
                  <p class="text-muted text-small mb-1">Zipcode :</p>
                  <p>
                    101110
                  </p>
                </div>
              </div>



            </div>
          </div>

          <div class="mb-2 d-flex justify-content-between align-items-center">
						<div class="col-l">
							<h2 class="sub-head font-weight-bold text-medium mb-0">Consent List</h2>
						</div>
						<div class="col-r pb-2 top-right-button-container d-flex align-items-end">
                            <a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#customerAddPop" class="btn btn-primary btn-md top-right-button mr-1">+ Add</a>
								
						</div>
					</div>


					<div class="card main-consent-setting">
					<div class="card-body">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
							<!--<table id="tablelist" class="data-table data-table-feature">-->
							<!--<table id="tablelist" class="data-table data-table-standard">-->
                           <table class="data-table data-tables-pagination responsive nowrap">
							
									<thead>
										<tr>
											<th class="text-center pr-0">No.</th>
											<th>consent</th>
                      <th width="15%">Version</th>
											<!--th class="text-center pr-0">Status</th-->
											<th class="text-center sort-none" width="25%">Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-center">1</td>
											<td>
												<a class="text-dark" href="#">
													XXX													
												</a>
											</td>
                      <td>
												5.0
											</td>
											<!--td class="text-center"><span class="text-primary font-weight-semibold">Active</span></td-->
											<td class="text-center">
											<a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#customerEditPop" class="btn btn-primary btn-sm mr-2">View</a> 
											<a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#customerEditPop" class="btn btn-primary btn-sm mr-2">Edit</a> 
											<a href="javascript:;" class="btn btn-outline-primary btn-sm">Delete</a>
											</td>
										</tr>

                    <tr>
											<td class="text-center">2</td>
											<td>
												<a class="text-dark" href="#">
													XXY													
												</a>
											</td>
                      <td>
												5.0
											</td>
											<!--td class="text-center"><span class="text-primary font-weight-semibold">Active</span></td-->
											<td class="text-center">
											<a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#customerEditPop" class="btn btn-primary btn-sm mr-2">View</a> 
											<a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#customerEditPop" class="btn btn-primary btn-sm mr-2">Edit</a> 
											<a href="javascript:;" class="btn btn-outline-primary btn-sm">Delete</a>
											</td>
										</tr>

                    <tr>
											<td class="text-center">3</td>
											<td>
												<a class="text-dark" href="#">
													XXZ												
												</a>
											</td>
                      <td>
												5.0
											</td>
											<!--td class="text-center"><span class="text-primary font-weight-semibold">Active</span></td-->
											<td class="text-center">
											<a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#customerEditPop" class="btn btn-primary btn-sm mr-2">View</a> 
											<a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#customerEditPop" class="btn btn-primary btn-sm mr-2">Edit</a> 
											<a href="javascript:;" class="btn btn-outline-primary btn-sm">Delete</a>
											</td>
										</tr>

                    						
																				

									</tbody>
								</table>
								
		

						</div>
					</div>
					</div>
                </div>
            </div>
        </div>

        <!-- add modal -->
          <div class="modal fade show" id="customerAddPop">
              <div class="modal-dialog" role="document">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <form>
                      <div class="modal-body">
                          
                            <div class="form-group cv-select-cr">
                              <label>Channal :</label>
                              <select class="form-control select2-single" data-width="100%">
                                <option label="&nbsp;">All  Channal</option>
                                <option value="Option 1">Option 1</option>
                                <option value="Option 2">Option 2</option>
                                <option value="Option 3">Option 3</option>
                              </select>
                            </div>

                            <div class="form-group cv-select-cr">
                              <label>Consent :</label>
                              <select class="form-control select2-single" data-width="100%">
                                <option label="&nbsp;">All  consent category</option>
                                <option value="Option 1">Option 1</option>
                                <option value="Option 2">Option 2</option>
                                <option value="Option 3">Option 3</option>
                              </select>
                            </div>

                            <div class="form-group">
                              <label>Detail :</label>
								              <textarea class="form-control" rows="2" name="jQueryDetail" required=""></textarea>
                            </div>
                          
                          
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                          <button type="button" class="btn btn-primary">Submit</button>
                      </div>
                      </form>
                  </div>
              </div>
          </div>
        <!-- /add modal -->

        <!-- Edit modal -->
          <div class="modal fade show" id="customerEditPop">
              <div class="modal-dialog" role="document">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Edit Consent</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <form>
                      <div class="modal-body">
                          
                            <div class="form-group cv-select-cr">
                              <label>Channal :</label>
                              <select class="form-control select2-single" data-width="100%">
                                <option label="&nbsp;">All  Channal</option>
                                <option value="Option 1" selected>Option 1</option>
                                <option value="Option 2">Option 2</option>
                                <option value="Option 3">Option 3</option>
                              </select>
                            </div>

                            <div class="form-group cv-select-cr">
                              <label>Consent :</label>
                              <select class="form-control select2-single" data-width="100%">
                                <option label="&nbsp;">All  consent category</option>
                                <option value="Option 1">Option 1</option>
                                <option value="Option 2" selected>Option 2</option>
                                <option value="Option 3">Option 3</option>
                              </select>
                            </div>

                            <div class="form-group">
                              <label>Detail :</label>
								              <textarea class="form-control" rows="2" name="jQueryDetail" required="">ตกลงยินยอมให้ บมจ.xxxx เปิดผข้อมูลส่วนบุกกของช้พจ เช่น ชื่อ ช่องทางในการติดต่อเพศอายุการศึกษา เป็นต้น แต่ทั้งนี้ ไม่รวมถึงข้อมูลที่เกี่ยวกับบัญชีของข้พเจ้าเช่น เลขที่บัญชี ยอดคงเหลือในบัญชี รายการเคลื่อนไหวในบัญชีเป็นต้น ให้แก่กลุ่มธุรกิจทางการเงินของธนาคารและให้กลุ่มธุรกิจทางการเงินของธนาคารสามารถใช้ข้มูลดังกล่ว พื่อวัตถุประสงค์ทางการตลาด เช่น เพื่อวัตถุประสงค์ในการพิจารณานำเสนอผลิตภัณฑ์ หรือเพื่อส่งเสริมการขายผลิตภัณฑ์บริกร และข้อสนอพิเศษอื่นๆ ของบริษัทในกลุ่มธุรกิจการเงินของธนาคาร ให้แก่ข้าพเจ้า </textarea>
                            </div>
                          
                          
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                          <button type="button" class="btn btn-primary">Submit</button>
                      </div>
                      </form>
                  </div>
              </div>
          </div>
        <!-- /Edit modal -->

    </main>
    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
</body>

</html>