<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>Dashboard</h1>
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">Dashboard</li>
							</ol>
						</nav>
                        <!--<div class="top-right-button-container">

                            <div class="btn-group">
                                <div class="btn btn-primary btn-lg pl-4 pr-0 check-button">
                                    <label class="custom-control custom-checkbox mb-0 d-inline-block">
                                        <input type="checkbox" class="custom-control-input" id="checkAll">
                                        <span class="custom-control-label">&nbsp;</span>
                                    </label>
                                </div>
                                <button type="button"
                                    class="btn btn-lg btn-primary dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                </div>
                            </div>
                        </div>-->
                    </div>

                    <div class="mb-2 d-flex justify-content-between">
						<div class="col-l">
							<a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
								role="button" aria-expanded="true" aria-controls="displayOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="collapse d-md-block" id="displayOptions">
								<div class="d-flex align-items-end">
									<!--<div class="float-md-left mr-1 mb-1 dropdown-as-select">
										<label class="d-block">Start Date :</label>
										<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											All Chanel
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item active" href="#">All Chanel</a>
											<a class="dropdown-item" href="#">Another action</a>
										</div>
									</div>

									<div class="float-md-left mr-1 mb-1 dropdown-as-select">
										<label class="d-block">End Date :</label>
										<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											All Application Services
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item active" href="#">All Application Services</a>
											<a class="dropdown-item" href="#">Another action</a>
										</div>
									</div>-->
									
									<div class="input-daterange no-gutters d-flex flex-nowrap align-items-end mr-2 mb-1" id="datepicker-report"> 
										<div class="col mb-0">
											<label class="d-block">Start date :</label>
											<input type="text" class="form-control form-control-sm bg-white rounded-2" name="Select date"
														placeholder="Start" value="05/04/2020" />
										</div>
										<span class="form-group pl-2 pr-2 mb-0"><label>to</label></span>
										<div class="col mb-0">
											<label class="d-block">End date :</label>
											<input type="text" class="form-control form-control-sm bg-white rounded-2" name="Select date"
														placeholder="End" value="05/15/2020" />
										</div>
									</div>

									<div class="float-md-left mr-2 mb-1 dropdown-as-select">
										<label class="d-block">Consent Group :</label>
										<button class="btn bg-white btn-xs dropdown-toggle" type="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											All Groups
									</button>
										<div class="dropdown-menu">
											<a class="dropdown-item active" href="#">All Groups</a>
											<a class="dropdown-item" href="#">Another action</a>
										</div>
									</div>
									
									<div class="float-md-left mr-1 mb-1">
										
										<button class="btn btn-primary btn-xs text-white" type="button">
											Search
										</button>
										
									</div>

								</div>
							</div>
						</div>
						<!--<div class="col-r pb-2 top-right-button-container d-flex align-items-end">
                            <button type="button" class="btn bg-green btn-lg top-right-button btn-export mr-1"><i class="glyph-icon iconsminds-receipt-4"></i> Export  Excel</button>

						</div>-->
					</div>

                    <div class="separator mb-5"></div>
					
					<h5 class="card-title mb-4">Fully Complete Status</h5>
				<div class="row mb-5">
						<div class="col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="dashboard-donut-chart chart">
										<canvas id="pieChart1"></canvas>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="dashboard-donut-chart chart">
									<canvas id="pieChart2"></canvas>
									</div>
								</div>
							</div>
						</div>

				</div>
					
				<h5 class="card-title mb-4">Consent Status </h5>
				<div class="card mb-5">
						<div class="card-header pt-3">
							<ul class="nav nav-tabs separator-tabs ml-0 mb-5" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="first-tab" data-toggle="tab" href="#first" role="tab"
										aria-controls="first" aria-selected="true">Superconsent ข้อ 1</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab"
										aria-controls="two" aria-selected="false">Superconsent ข้อ 2</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="third-tab" data-toggle="tab" href="#third" role="tab"
										aria-controls="third" aria-selected="false">Superconsent ข้อ 3</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="four-tab" data-toggle="tab" href="#four" role="tab"
										aria-controls="four" aria-selected="false">Superconsent ข้อ 4</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="five-tab" data-toggle="tab" href="#five" role="tab"
										aria-controls="five" aria-selected="false">Superconsent ข้อ 5</a>
								</li>
							</ul>
								

							<!--<div class="float-left float-none-xs">
								<div class="d-inline-block">
									<h5 class="d-inline">Consent Graph</h5>
								</div>
							</div>
							<div class="btn-group float-right float-none-xs mt-2">
								<button class="btn btn-outline-primary btn-xs dropdown-toggle" type="button"
									data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									This Week
								</button>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="#">Last Week</a>
									<a class="dropdown-item" href="#">This Month</a>
								</div>
							</div>-->

						</div>
						<div class="card-body tab-content">
							<div class="tab-pane show active" id="first" role="tabpanel" aria-labelledby="first-tab">
								<div class="dashboard-line-chart chart">
								<canvas id="supChart1"></canvas>
								</div>
							</div>
							
							<div class="tab-pane fade" id="two" role="tabpanel" aria-labelledby="third-two">
							2
							</div>
							
							<div class="tab-pane fade" id="third" role="tabpanel" aria-labelledby="third-tab">
							3
							</div>
							
							<div class="tab-pane fade" id="four" role="tabpanel" aria-labelledby="four-tab">
							4
							</div>
							
							<div class="tab-pane fade" id="five" role="tabpanel" aria-labelledby="five-tab">
							5
							</div>
						</div>
					</div>
					
					<h5 class="card-title mb-4">Consent Status by Region (Agreed only)</h5>
				<div class="row mb-5">
						<div class="col-sm-6">
							<div class="card">
								<div class="card-body pb-3">
									<div class="dashboard-donut-chart chart">
									<h5 class="h5 text-center">Latest Version</h5>
									<canvas id="RegionChart1"></canvas>
									</div>
								</div>
							</div>
				</div>
						
						<div class="col-sm-6">
							<div class="card">
								<div class="card-body pb-3">
									<div class="dashboard-donut-chart chart">
									<h5 class="h5 text-center">Previous Version</h5>
									<canvas id="RegionChart2"></canvas>
									</div>
								</div>
							</div>
						</div>

				</div>
            </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	
<script src="js/vendor/Chart.bundle.min.js"></script>
<script src="js/vendor/chartjs-plugin-datalabels.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	
	/* 03.01. Getting Colors from CSS */
    /*var rootStyle = getComputedStyle(document.body);
    var themeColor1 = rootStyle.getPropertyValue("--theme-color-1").trim();
    var themeColor2 = rootStyle.getPropertyValue("--theme-color-2").trim();
    var themeColor3 = rootStyle.getPropertyValue("--theme-color-3").trim();
    var themeColor4 = rootStyle.getPropertyValue("--theme-color-4").trim();
    var themeColor5 = rootStyle.getPropertyValue("--theme-color-5").trim();
    var themeColor6 = rootStyle.getPropertyValue("--theme-color-6").trim();
    var themeColor1_10 = rootStyle
      .getPropertyValue("--theme-color-1-10")
      .trim();
    var themeColor2_10 = rootStyle
      .getPropertyValue("--theme-color-2-10")
      .trim();
    var themeColor3_10 = rootStyle
      .getPropertyValue("--theme-color-3-10")
      .trim();
    var themeColor4_10 = rootStyle
      .getPropertyValue("--theme-color-4-10")
      .trim();

    var themeColor5_10 = rootStyle
      .getPropertyValue("--theme-color-5-10")
      .trim();
    var themeColor6_10 = rootStyle
      .getPropertyValue("--theme-color-6-10")
      .trim();

    var primaryColor = rootStyle.getPropertyValue("--primary-color").trim();
    var foregroundColor = rootStyle
      .getPropertyValue("--foreground-color")
      .trim();
    var separatorColor = rootStyle.getPropertyValue("--separator-color").trim();*/
	
/*chart 1*/		
var chartTooltip = {
        /*backgroundColor: '#fff',
        titleFontColor: '#0766AC',
        borderColor: '##0766AC',
        borderWidth: 0.5,
        bodyFontColor: '#fff',*/
        bodySpacing: 10,
        xPadding: 15,
        yPadding: 15,
        cornerRadius: 0.15,
        displayColors: false
      };
	  
if (document.getElementById("pieChart1")) {
        var pieChart = document.getElementById("pieChart1");
        var myChart = new Chart(pieChart, {
          type: "pie",
          data: {
            labels: ["Latest Version Fully Complete", "Not Fully Complete"],
            datasets: [
              {
                label: "",
                //borderColor: [themeColor1, themeColor2, themeColor3],
                backgroundColor: [
                  '#01a1ff',
                  '#60d837',
                 // '#0ff'
                ],
                borderWidth: 2,
                data: [24.94, 75.06]
              }
            ]
          },
          draw: function () { },
          options: {
            plugins: {
              datalabels: {
                display: false
              }
            },
            responsive: true,
            maintainAspectRatio: false,
            title: {
              display: false
            },
            layout: {
              padding: {
                bottom: 20
              }
            },
            legend: {
              position: "bottom",
              labels: {
                padding: 30,
                usePointStyle: true,
                fontSize: 12
              }
            },
            tooltips: chartTooltip
          }
        });
      }
	  
	if (document.getElementById("pieChart2")) {
        var pieChart = document.getElementById("pieChart2");
        var myChart = new Chart(pieChart, {
          type: "pie",
          data: {
            labels: ["Previous Version Fully Complete", "Not Fully Complete"],
            datasets: [
              {
                label: "",
                //borderColor: [themeColor1, themeColor2, themeColor3],
                backgroundColor: [
                  '#01a1ff',
                  '#60d837',
                 // '#0ff'
                ],
                borderWidth: 2,
                data: [33.22, 66.78]
              }
            ]
          },
          draw: function () { },
          options: {
            plugins: {
              datalabels: {
                display: false
              }
            },
            responsive: true,
            maintainAspectRatio: false,
            title: {
              display: false
            },
            layout: {
              padding: {
                bottom: 20
              }
            },
            legend: {
              position: "bottom",
              labels: {
                padding: 30,
                usePointStyle: true,
                fontSize: 12
              }
            },
            tooltips: chartTooltip
          }
        });
      }
	
	if (document.getElementById("supChart1")) {
        var supChart1 = document
          .getElementById("supChart1")
          .getContext("2d");
        var myChart = new Chart(supChart1, {
          type: "bar",
          options: {
            plugins: {
              datalabels: {
                display: false
              }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
              yAxes: [
                {
                  gridLines: {
                    display: true,
                    lineWidth: 1,
                    color: "rgba(0,0,0,0.1)",
                    drawBorder: false
                  },
                  ticks: {
                    beginAtZero: true,
                    stepSize: 50,
                    min: 10,
                    max: 300,
                    padding: 10
                  }
                }
              ],
              xAxes: [
                {
                  gridLines: {
                    display: false
                  }
                }
              ]
            },
            legend: {
              position: "bottom",
              labels: {
                padding: 30,
                usePointStyle: true,
                fontSize: 12
              }
            },
            //tooltips: chartTooltip
          },
          data: {
            labels: ["Latest Version", "Previous Version"],
            datasets: [
              {
                label: "Agree",
                borderColor: '#545454',
                backgroundColor: '#01a1ff',
                data: [200, 300],
                borderWidth: 2
              },
              {
                label: "Disagree",
                borderColor: '#545454',
                backgroundColor: '#60d837',
                data: [80, 100],
                borderWidth: 2
              },
              {
                label: "Not Answer",
                borderColor: '#545454',
                backgroundColor: '#919191',
                data: [20, 150],
                borderWidth: 2
              },
              {
                label: "Revoked",
                borderColor: '#545454',
                backgroundColor: '#f8ba00',
                data: [1, 2],
                borderWidth: 2
              },
              {
                label: "Not yet Asked",
                borderColor: '#545454',
                backgroundColor: '#fa2500',
                data: [100, 50],
                borderWidth: 2
              }
            ]
          }
        });
      }
	  
	  if (document.getElementById("RegionChart1")) {
        var RegionChart1 = document
          .getElementById("RegionChart1")
          .getContext("2d");
        var myChart = new Chart(RegionChart1, {
          type: "bar",
          options: {
            plugins: {
              datalabels: {
                display: false
              }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
              yAxes: [
                {
                  gridLines: {
                    display: true,
                    lineWidth: 1,
                    color: "rgba(0,0,0,0.1)",
                    drawBorder: false
                  },
                  ticks: {
                    beginAtZero: true,
                    stepSize: 100,
                    min: 0,
                    max: 800,
                    padding: 10
                  },
				  stacked: true
                }
              ],
              xAxes: [
                {
                  gridLines: {
                    display: false
                  },
				  stacked: true
                }
              ]
            },
            legend: {
              position: "bottom",
              labels: {
                padding: 30,
                usePointStyle: true,
                fontSize: 12
              }
            },
            //tooltips: chartTooltip
          },
          data: {
            labels: ["1001008", "1001013", "1001003", "1001012", "1001002", "1001001", "1001011", "1001014", "1001015", "1001010"],
            datasets: [
              {
                label: "Superconsent 1",
                borderColor: '#545454',
                backgroundColor: '#01a1ff',
                data: [100, 130, 80, 60, 150, 100, 90, 90, 70, 50],
              },
              {
                label: "Superconsent 2",
                borderColor: '#545454',
                backgroundColor: '#60d837',
                data: [100, 130, 80, 60, 150, 100, 90, 90, 70, 50],
              },
              {
                label: "Superconsent 3",
                borderColor: '#545454',
                backgroundColor: '#919191',
                data: [100, 130, 80, 60, 150, 100, 90, 90, 70, 50],
              },
              {
                label: "Superconsent 4",
                borderColor: '#545454',
                backgroundColor: '#f8ba00',
               data: [100, 130, 80, 60, 150, 100, 90, 90, 70, 50],
              },
              {
                label: "Superconsent 5",
                borderColor: '#545454',
                backgroundColor: '#fa2500',
                data: [100, 130, 80, 60, 150, 100, 90, 90, 70, 50],
              }
            ]
          }
        });
      }
	  
	  if (document.getElementById("RegionChart2")) {
        var RegionChart2 = document
          .getElementById("RegionChart2")
          .getContext("2d");
        var myChart = new Chart(RegionChart2, {
          type: "bar",
          options: {
            plugins: {
              datalabels: {
                display: false
              }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
              yAxes: [
                {
                  gridLines: {
                    display: true,
                    lineWidth: 1,
                    color: "rgba(0,0,0,0.1)",
                    drawBorder: false
                  },
                  ticks: {
                    beginAtZero: true,
                    stepSize: 175,
                    min: 0,
                    max: 700,
                    padding: 10
                  },
				  stacked: true
                }
              ],
              xAxes: [
                {
                  gridLines: {
                    display: false
                  },
				  stacked: true
                }
              ]
            },
            legend: {
              position: "bottom",
              labels: {
                padding: 30,
                usePointStyle: true,
                fontSize: 12
              }
            },
            //tooltips: chartTooltip
          },
          data: {
            labels: ["1001008", "1001013", "1001003", "1001012", "1001002", "1001001", "1001011", "1001014", "1001015", "1001010"],
            datasets: [
              {
                label: "Superconsent 1",
                borderColor: '#545454',
                backgroundColor: '#01a1ff',
                data: [80, 100, 50, 40, 140, 90, 70, 60, 30, 40],
              },
              {
                label: "Superconsent 2",
                borderColor: '#545454',
                backgroundColor: '#60d837',
                data: [80, 100, 50, 40, 140, 90, 70, 60, 30, 40],
              },
              {
                label: "Superconsent 3",
                borderColor: '#545454',
                backgroundColor: '#919191',
                data: [80, 100, 50, 40, 140, 90, 70, 60, 30, 40],
              },
              {
                label: "Superconsent 4",
                borderColor: '#545454',
                backgroundColor: '#f8ba00',
                data: [80, 100, 50, 40, 140, 90, 70, 60, 30, 40],
              },
              {
                label: "Superconsent 5",
                borderColor: '#545454',
                backgroundColor: '#fa2500',
                data: [80, 100, 50, 40, 140, 90, 70, 60, 30, 40],
              }
            ]
          }
        });
      }


	</script>
<script>
$(document).ready(function() {
	$('.main-menu .list-unstyled>li').removeClass('active');
	$('.main-menu .list-unstyled>li:nth-child(1)').addClass('active');
});
</script>
</body>

</html>