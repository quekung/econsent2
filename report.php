<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>Report</h1>
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">Consent System Report </li>
							</ol>
						</nav>
                        <!--<div class="top-right-button-container">

                            <div class="btn-group">
                                <div class="btn btn-primary btn-lg pl-4 pr-0 check-button">
                                    <label class="custom-control custom-checkbox mb-0 d-inline-block">
                                        <input type="checkbox" class="custom-control-input" id="checkAll">
                                        <span class="custom-control-label">&nbsp;</span>
                                    </label>
                                </div>
                                <button type="button"
                                    class="btn btn-lg btn-primary dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                </div>
                            </div>
                        </div>-->
                    </div>

                    <div class="mb-2 d-flex justify-content-between">
						<div class="col-l">
							<a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
								role="button" aria-expanded="true" aria-controls="displayOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="collapse d-md-block" id="displayOptions">
								<div class="d-flex align-items-end">
									<!--<div class="float-md-left mr-1 mb-1 dropdown-as-select">
										<label class="d-block">Start Date :</label>
										<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											All Chanel
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item active" href="#">All Chanel</a>
											<a class="dropdown-item" href="#">Another action</a>
										</div>
									</div>

									<div class="float-md-left mr-1 mb-1 dropdown-as-select">
										<label class="d-block">End Date :</label>
										<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											All Application Services
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item active" href="#">All Application Services</a>
											<a class="dropdown-item" href="#">Another action</a>
										</div>
									</div>-->
									
									<div class="input-daterange no-gutters d-flex flex-nowrap align-items-end mr-2 mb-1" id="datepicker-report"> 
										<div class="col mb-0">
											<label class="d-block">Start date :</label>
											<input type="text" class="form-control form-control-sm bg-white rounded-2" name="Select date"
														placeholder="Start" value="05/04/2020" />
										</div>
										<span class="form-group pl-2 pr-2 mb-0"><label>to</label></span>
										<div class="col mb-0">
											<label class="d-block">End date :</label>
											<input type="text" class="form-control form-control-sm bg-white rounded-2" name="Select date"
														placeholder="End" value="05/15/2020" />
										</div>
									</div>

									<div class="float-md-left mr-2 mb-1 dropdown-as-select">
										<label class="d-block">System :</label>
										<button class="btn bg-white btn-xs dropdown-toggle" type="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											All System
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item active" href="#">All System</a>
											<a class="dropdown-item" href="#">Another action</a>
										</div>
									</div>
									
									<div class="float-md-left mr-1 mb-1">
										
										<button class="btn btn-primary btn-xs text-white" type="button">
											Search
										</button>
										
									</div>

								</div>
							</div>
						</div>
						<div class="col-r pb-2 top-right-button-container d-flex align-items-end">
                            <button type="button" class="btn bg-green btn-lg top-right-button btn-export mr-1"><i class="glyph-icon iconsminds-receipt-4"></i> Export  Excel</button>

						</div>
					</div>

                    <div class="separator mb-5"></div>
					
					<h5 class="card-title mb-4">Consent System  Report  between  1-Jan-2020  to   31-Jan-2020 </h5>
					
					<div class="card">
					<div class="card-body">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
							<!--<table id="tablelist" class="data-table data-table-feature">-->
							<!--<table id="tablelist" class="data-table data-table-standard">-->
                           <table class="data-table dataTable no-footer responsive nowrap">
							
									<thead>
										<tr>
											<th>No.</th>
											<th>Date</th>
											<th>Product Name</th>
											<th>System Name</th>
											<th>Channel Input</th>
											<th>Consent Category</th>
											<th>Customer name</th>
										</tr>
									</thead>
									<tbody>
										<?php for($i=1;$i<=20;$i++){ ?>
										<tr>
											<td><?php echo $i ?></td>
											<td>1-Jan-2020</td>
											<td>Loan</td>
											<td>
												<a class="text-dark" href="#">
													MBK loan system management													
												</a>
											</td>
											<td>Call API</td>
											<td><a class="text-dark" href="#">หนังสือให้ความยินยอมเปิดเผยข้อมูล</a></td>
											<td><a class="text-dark" href="#">นาย กิตติ   นามสมมุติ</a></td>
										</tr>
										<?php } ?>
										<!-- modal -->
										<div class="modal fade" id="viewProfile" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title">Profile</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">×</span>
													</button>
												</div>
												<div class="modal-body">
													<ul class="list-inline">
														<li class="mb-4">
															<a class="p-0 d-flex align-items-center text-dark" href="#">
																<span class="mr-2">
																	<img alt="Profile Picture" src="img/profile-pic-l.jpg" class="rounded-circle" height="100">
																</span>
																<h2 class="name text-left">
																	เอสเธอร์ อเล็กซานเดอร์
																	<small class="d-block">Esther Alexander</small>
																</h2>

															</a>
														</li>
														<li>
															<div class="form-group">
																<label>Email</label>
																<input type="text" class="form-control" placeholder="" value="lillie.foster@example.com">
															</div>
														</li>
														<li>
															<div class="form-group">
																<label>Mobile Number</label>
																<input type="text" class="form-control" placeholder="" value="302.555.0107">
															</div>
														</li>
														<li>
															<div class="form-group">
																<label>ID Card</label>
																<input type="text" class="form-control" placeholder="" value=" 	6783776898788">
															</div>
														</li>
														
													</ul>

												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
													<button type="button" class="btn btn-primary">Save changes</button>
												</div>
											</div>
										</div>
									</div>
									<!-- /modal -->

									</tbody>
								</table>
								
		

						</div>
						
						<nav class="mt-4 mb-3">
                        <ul class="pagination justify-content-center mb-0">
                            <li class="page-item ">
                                <a class="page-link first" href="#">
                                    <i class="simple-icon-control-start"></i>
                                </a>
                            </li>
                            <li class="page-item ">
                                <a class="page-link prev" href="#">
                                    <i class="simple-icon-arrow-left"></i>
                                </a>
                            </li>
                            <li class="page-item active">
                                <a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item ">
                                <a class="page-link" href="#">2</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">3</a>
                            </li>
                            <li class="page-item ">
                                <a class="page-link next" href="#" aria-label="Next">
                                    <i class="simple-icon-arrow-right"></i>
                                </a>
                            </li>
                            <li class="page-item ">
                                <a class="page-link last" href="#">
                                    <i class="simple-icon-control-end"></i>
                                </a>
                            </li>
                        </ul>
                    </nav>
					</div>
					</div>
                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
		<script>
$(document).ready(function() {
	$('.main-menu .list-unstyled>li').removeClass('active');
	$('.main-menu .list-unstyled>li:nth-child(4)').addClass('active');
});
</script>
</body>

</html>