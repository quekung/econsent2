<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>
		<main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>User Management</h1>
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">User </li>
							</ol>
						</nav>

                    </div>

                    <div class="mb-2 d-flex justify-content-between">
						<div class="col-l">
							<a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
								role="button" aria-expanded="true" aria-controls="displayOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="collapse d-md-block" id="displayOptions">
								<div class="d-flex align-items-end">
                  <div class="float-md-left mr-1 mb-1 dropdown-as-select">
										<label class="d-block">Role Name :</label>
										<button class="btn bg-white btn-outline-dark btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											All Role
										</button>
										<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(15px, 116px, 0px); top: 0px; left: 0px; will-change: transform;">
											<a class="dropdown-item active" href="#">All Role</a>
											<a class="dropdown-item" href="#">Super Admin</a>
                      <a class="dropdown-item" href="#">Admin</a>
                      <a class="dropdown-item" href="#">User</a>
                      <a class="dropdown-item" href="#">Viewer</a>
										</div>
									</div>

									<div class="float-md-left mr-1 mb-1">
										<label class="d-block">Userame :</label>
										<input type="text" class="form-control form-control-sm bg-white rounded-2" name="UserName" placeholder="">
									</div>
									<div class="float-md-left mr-1 mb-1">
										
										<button class="btn btn-primary btn-xs text-white" type="button">
											Search
										</button>
										
									</div>
									

								</div>
							</div>
						</div>
						<div class="col-r pb-2 top-right-button-container d-flex align-items-end">
                            <a href="/user-add" class="btn btn-primary btn-md top-right-button mr-1">+ Add role</a>
								
						</div>
					</div>

                    <div class="mb-3"></div>
					<div class="card main-consent-setting">
					<div class="card-body">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
							<!--<table id="tablelist" class="data-table data-table-feature">-->
							<!--<table id="tablelist" class="data-table data-table-standard">-->
                           <table class="data-table data-tables-pagination responsive nowrap">
							
									<thead>
										<tr>
											<th class="text-center pr-0" width="10%">No.</th>
											<th>UserName</th>
                      <th>Email</th>
											<th class="text-center pr-0" width="15%">Status</th>
											<th class="text-center sort-none" width="25%">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php for($i=1;$i<=30;$i++){ ?>
										<tr>
											<td class="text-center"><?php echo $i ?></td>
											<td>
												<a class="p-0 d-flex" href="#">
													<span class="mr-2">
														<img alt="Profile Picture" src="img/profile-pic-l.jpg" class="rounded-circle" height="40">
													</span>
													<span class="name text-left">
														เอสเธอร์ อเล็กซานเดอร์
														<small class="d-block">Esther Alexander</small>
													</span>
													
												</a>

											</td>
                      <td><a href="mailto:lillie.foster@example.com">lillie.foster@example.com</a></td>
											<td class="text-center"><span class="text-primary font-weight-semibold">Active</span></td>
											<td class="text-center">
											<a href="/user-edit" class="btn btn-primary btn-sm mr-2">Edit</a> 
											<a href="javascript:;" class="btn btn-outline-primary btn-sm">Delete</a>
											</td>
										</tr>
                    					<?php } ?>

										
																				
																				
																				

									</tbody>
								</table>
								
		

						</div>
					</div>
					</div>
                </div>
            </div>
        </div>


    </main>
    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
</body>

</html>